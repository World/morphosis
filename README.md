> This project is in maintenance mode. I will keep updating the Pandoc version and the runtime, but don't expect new features.

<div align="center">
  <img src="data/icons/hicolor/scalable/apps/garden.jamie.Morphosis.svg" width="128" height="128">

  # Morphosis

  Convert your documents

  <img src="data/screenshots/1.png">
</div>

## The Project

Morphosis is a document conversion app written in Python, using GTK4 and Libadwaita. Conversions are done with [Pandoc](https://pandoc.org/) and [WebKitGTK](https://webkitgtk.org/).

## Features

For the supported import formats, see [Pandoc's manual](https://pandoc.org/MANUAL.html#option--from). The supported export formats are:

- PDF
- Markdown
- reStructuredText
- LaTeX
- HTML
- Microsoft Word (.docx)
- OpenOffice/LibreOffice (.odt)
- Rich Text Format (.rtf)
- EPUB
- AsciiDoc

## Installation

<a href="https://flathub.org/apps/garden.jamie.Morphosis">
  <img width="240" alt="Download on Flathub" src="https://flathub.org/api/badge?locale=en&light"/>
</a>

## FAQ

### Why are images not working?

Morphosis uses a limited set of permissions by default to ensure it's secure. This means that the app can't access the network, nor can it install additional media.

## Code of Conduct

The project follows the [GNOME Code of Conduct](https://conduct.gnome.org/).

## Translations

This project is translated with [Weblate](https://hosted.weblate.org/engage/morphosis/).
