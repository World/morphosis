<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
  <id>garden.jamie.Morphosis</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0-or-later</project_license>
  <name>Morphosis</name>
  <summary>Convert your documents</summary>
  <developer id="garden.jamie">
    <name translate="no">Jamie Gravendeel</name>
  </developer>
  <description>
    <p>Use Morphosis to easily convert your text documents to other formats.</p>
    <p>Supported export formats:</p>
    <ul>
      <li>PDF</li>
      <li>Markdown</li>
      <li>reStructuredText</li>
      <li>LaTeX</li>
      <li>HTML</li>
      <li>Microsoft Word (.docx)</li>
      <li>OpenOffice/LibreOffice (.odt)</li>
      <li>Rich Text Format (.rtf)</li>
      <li>EPUB</li>
      <li>AsciiDoc</li>
    </ul>
  </description>
  <launchable type="desktop-id">garden.jamie.Morphosis.desktop</launchable>
  <branding>
    <color type="primary" scheme_preference="light">#8ff0a4</color>
    <color type="primary" scheme_preference="dark">#167158</color>
  </branding>
  <content_rating type="oars-1.1" />
  <url type="bugtracker">https://gitlab.gnome.org/World/morphosis/-/issues</url>
  <url type="homepage">https://gitlab.gnome.org/World/morphosis</url>
  <url type="contact">https://jamie.garden</url>
  <url type="vcs-browser">https://gitlab.gnome.org/World/morphosis</url>
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <supports>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </supports>
  <screenshots>
    <screenshot type="default">
      <image type="source" width="632" height="632">https://gitlab.gnome.org/World/morphosis/-/raw/v1.4.1/data/screenshots/1.png</image>
    </screenshot>
  </screenshots>
  <releases>
    <release version="1.4.1" date="2024-09-18"/>
    <release version="1.4" date="2024-09-18">
      <description translate="no">
        <ul>
          <li>Added translations</li>
          <li>AsciiDoc support</li>
          <li>New font type explanation</li>
          <li>Updated to GNOME 47</li>
        </ul>
      </description>
    </release>
    <release version="1.3" date="2024-06-13"/>
    <release version="1.2" date="2024-06-03">
      <description translate="no">
        <ul>
          <li>Documents are now properly wrapped</li>
        </ul>
      </description>
    </release>
    <release version="1.1" date="2024-05-21">
      <description translate="no">
        <ul>
          <li>You can now choose a font style for PDF and HTML</li>
          <li>Images in PDF and HTML now show up correctly</li>
        </ul>
      </description>
    </release>
    <release version="1.0" date="2024-05-18">
      <description translate="no">
        <p>Initial release</p>
      </description>
    </release>
  </releases>
</component>
