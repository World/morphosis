# main.py
#
# Copyright 2024 Jamie Gravendeel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""The main application singleton class."""
import sys
from typing import Any, Callable

import gi

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")
gi.require_version("WebKit", "6.0")

# pylint: disable=wrong-import-position

from gi.repository import Adw, Gio

from .window import MorphosisWindow


class MorphosisApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self) -> None:
        super().__init__(
            application_id="garden.jamie.Morphosis",
            flags=Gio.ApplicationFlags.DEFAULT_FLAGS,
        )
        self.__create_action("quit", lambda *_: self.quit(), ("<primary>q",))
        self.__create_action(
            "close",
            lambda *_: window.close() if (window := self.get_active_window()) else None,
            ("<primary>w",),
        )
        self.__create_action(
            "open-document",
            lambda *_: (
                window.open_document()
                if (window := self.get_active_window())
                and window.stack.get_visible_child()
                != window.page_converting.get_child()
                else None
            ),
            ("<primary>o",),
        )
        self.__create_action("about", self.__on_about_action)

    def do_activate(self) -> None:  # pylint: disable=arguments-differ
        """Raises the application's main window, creates it if necessary."""
        win = self.get_active_window()
        if not win:
            win = MorphosisWindow(application=self)
        win.present()

    def __on_about_action(self, *_args: Any) -> None:
        """Callback for the app.about action."""
        about = Adw.AboutDialog.new_from_appdata(
            "/garden/jamie/Morphosis/garden.jamie.Morphosis.metainfo.xml"
        )
        about.set_developers(
            ("Jamie Gravendeel https://jamie.garden", "kramo https://kramo.page")
        )
        about.set_designers(("Jamie Gravendeel https://jamie.garden",))
        about.set_artists(
            (
                "kramo https://kramo.page",
                "Jakub Steiner https://jimmac.eu",
                "Tobias Bernard https://tobiasbernard.com",
            )
        )
        about.set_copyright("© 2024 Jamie Gravendeel")
        about.present(self.get_active_window())

    def __create_action(
        self, name: str, callback: Callable[..., None], shortcuts: tuple[str] = None
    ) -> None:
        """Add an application action."""
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(*_args: Any) -> int:
    """The application's entry point."""
    app = MorphosisApplication()
    return app.run(sys.argv)
