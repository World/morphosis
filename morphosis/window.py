# window.py
#
# Copyright 2024 Jamie Gravendeel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""The main application window."""
import logging
import shlex
from pathlib import Path
from typing import Any, Callable, Optional

from gi.repository import Adw, Gdk, Gio, GLib, Gtk

from morphosis import supported_formats
from morphosis.print import print_to_pdf


@Gtk.Template(resource_path="/garden/jamie/Morphosis/gtk/window.ui")
class MorphosisWindow(Adw.ApplicationWindow):
    """The main application window."""

    __gtype_name__ = "MorphosisWindow"

    toast_overlay: Adw.ToastOverlay = Gtk.Template.Child()

    stack: Gtk.Stack = Gtk.Template.Child()
    page_start: Gtk.StackPage = Gtk.Template.Child()
    page_document: Gtk.StackPage = Gtk.Template.Child()
    page_converting: Gtk.StackPage = Gtk.Template.Child()

    image_mimetype: Gtk.Image = Gtk.Template.Child()
    label_document: Gtk.Label = Gtk.Template.Child()
    row_format: Adw.ComboRow = Gtk.Template.Child()
    revealer_font: Gtk.Revealer = Gtk.Template.Child()
    row_font: Adw.ComboRow = Gtk.Template.Child()

    dialog_font_style = Gtk.Template.Child()
    dialog_help: Adw.Dialog = Gtk.Template.Child()

    title_why: Gtk.Label = Gtk.Template.Child()
    paragraph_why: Gtk.Label = Gtk.Template.Child()
    title_what: Gtk.Label = Gtk.Template.Child()
    paragraph_what: Gtk.Label = Gtk.Template.Child()

    document: Optional[Gio.File] = None
    document_path: Optional[str] = None
    document_name: Optional[str] = None
    document_suffix: Optional[str] = None

    schema = Gio.Settings.new("garden.jamie.Morphosis")

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self.row_format.set_selected(self.schema.get_int("export-format"))
        self.row_font.set_selected(self.schema.get_int("font-style"))

        # Make the labels accessible by screen readers
        self.dialog_help.update_property(
            (Gtk.AccessibleProperty.LABEL,),
            (
                self.title_why.get_label()
                + "\n"
                + self.paragraph_why.get_label()
                + "\n"
                + self.title_what.get_label()
                + "\n"
                + self.paragraph_what.get_label(),
            ),
        )

        for widget in (self.page_start.get_child(), self.page_document.get_child()):
            (drop_target := Gtk.DropTarget.new(Gio.File, Gdk.DragAction.COPY)).connect(
                "drop",
                lambda _target, document, _x, _y: self.__get_document(document),
            )
            widget.add_controller(drop_target)

    @Gtk.Template.Callback()
    def _show_font_style_dialog(self, *_args: Any) -> None:
        self.dialog_font_style.present(self)

    @Gtk.Template.Callback()
    def _export_format_changed(self, *_args: Any) -> None:
        self.schema.set_int("export-format", self.row_format.get_selected())
        self.__show_row_font()

    @Gtk.Template.Callback()
    def _font_style_changed(self, *_args: Any) -> None:
        self.schema.set_int("font-style", self.row_font.get_selected())

    def __get_format_has_font_opts(self) -> bool:
        if self.document_suffix == "html":
            return self.row_format.get_selected_item().get_string() == ("HTML")  # type: ignore
        return self.row_format.get_selected_item().get_string() in ("HTML", "PDF")

    def __show_row_font(self) -> None:
        if self.__get_format_has_font_opts():
            self.revealer_font.set_reveal_child(True)
            return

        self.revealer_font.set_reveal_child(False)

    def __add_toast(
        self,
        title: str,
        button_label: Optional[str] = None,
        action: Optional[Callable[..., None]] = None,
    ) -> None:
        """Creates a toast and shows it to the user."""
        toast = Adw.Toast.new(title)

        if button_label:
            toast.set_button_label(button_label)

        if action:
            toast.connect("button-clicked", action)

        toast.set_priority(Adw.ToastPriority.HIGH)
        self.toast_overlay.add_toast(toast)

    def open_document(self) -> None:
        """Opens a file dialog to select a document."""
        dialog = Gtk.FileDialog(title=_("Select Document"))
        file_filter = Gtk.FileFilter()

        for mime_type in supported_formats.MIME_TYPES:
            file_filter.add_mime_type(mime_type)

        for suffix in supported_formats.EXTENSIONS:
            file_filter.add_suffix(suffix)

        file_filter.set_name(_("Document"))

        filters = Gio.ListStore()
        filters.append(file_filter)
        dialog.set_filters(filters)
        dialog.set_default_filter(file_filter)

        dialog.open(self, callback=self.__open_document_cb)

    def __open_document_cb(self, dialog: Gtk.FileDialog, res: Gio.AsyncResult) -> None:
        try:
            if not (document := dialog.open_finish(res)):
                logging.error("Cannot get document")
                return

        except GLib.Error as error:
            logging.error("Cannot get document: %s", error)
            return

        self.__get_document(document)

    def __get_document(self, document: Gio.File) -> None:
        """Gets information about the document and verifies it."""
        if (
            document.query_info(
                Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                Gio.FileQueryInfoFlags.NONE,
            ).get_content_type()
            not in supported_formats.MIME_TYPES
            and Path(
                document.query_info(
                    Gio.FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME,
                    Gio.FileQueryInfoFlags.NONE,
                ).get_display_name()
            ).suffix[1:]
            not in supported_formats.EXTENSIONS
        ):
            logging.warning(
                "File format %s is not supported",
                document.query_info(
                    Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                    Gio.FileQueryInfoFlags.NONE,
                ).get_content_type(),
            )
            self.__add_toast(_("Unsupported file format"))
            return

        self.document_name = document.query_info(
            Gio.FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME, Gio.FileQueryInfoFlags.NONE
        ).get_display_name()
        self.document_suffix = Path(self.document_name).suffix[1:]
        self.document = document
        self.document_path = document.get_path()

        if self.document_suffix == "html":
            self.image_mimetype.set_from_icon_name("text-html")
        else:
            self.image_mimetype.set_from_icon_name("x-office-document")

        self.label_document.set_label(self.document_name)
        self.__show_row_font()
        self.stack.set_visible_child(self.page_document.get_child())

    @Gtk.Template.Callback()
    def _save_location(self, *_args: Any) -> None:
        """Opens a file dialog to select a save location."""
        ext = supported_formats.FORMATS[
            self.row_format.get_selected_item().get_string()
        ].ext

        dialog = Gtk.FileDialog(
            initial_name=f"{Path(name).stem if (name := self.document_name) else 'Document'}.{ext}"  # pylint: disable=used-before-assignment
        )

        dialog.save(self, callback=self.__save_location_cb)

    def __save_location_cb(self, dialog: Gtk.FileDialog, res: Gio.AsyncResult) -> None:
        try:
            if not (output_file := dialog.save_finish(res)):
                logging.error("Cannot get save location")
                return

        except GLib.Error as error:
            logging.error("Cannot get save location: %s", error)
            return

        self.stack.set_visible_child(self.page_converting.get_child())

        GLib.Thread.new(None, self.__convert, output_file)

    def __convert_success(
        self, output_file: Gio.File, tmp_file: Optional[Gio.File] = None
    ) -> None:
        if tmp_file:
            tmp_file.delete_async(GLib.PRIORITY_DEFAULT)

        GLib.idle_add(self.stack.set_visible_child, self.page_start.get_child())
        GLib.idle_add(
            self.__add_toast,
            _("Document converted"),
            _("Open"),
            lambda *_: Gtk.FileLauncher.new(output_file).launch(self),
        )

    def __convert_fail(self, tmp_file: Optional[Gio.File] = None) -> None:
        if tmp_file:
            tmp_file.delete_async(GLib.PRIORITY_DEFAULT)

        GLib.idle_add(self.stack.set_visible_child, self.page_start.get_child())
        GLib.idle_add(
            self.__add_toast,
            _("Conversion failed"),
            _("Help"),
            lambda *_: self.dialog_help.present(self),
        )

    def __convert(self, output_file: Gio.File) -> None:
        """Converts the document."""
        if not (output_path := output_file.get_path()):
            logging.error("Cannot get path for output file")
            return self.__convert_fail()

        if not (document_path := self.document_path):
            return

        document_format = supported_formats.FORMATS[
            self.row_format.get_selected_item().get_string()
        ].format

        font_arg = ""
        if self.__get_format_has_font_opts():
            if self.row_font.get_selected_item().get_string() == "Serif":
                font_arg = '-V mainfont="Serif"'
            else:
                font_arg = '-V mainfont="Sans-Serif"'

        pandoc_args_list = [
            font_arg,
            f"{shlex.quote(document_path)}",
            "-V lang=und",
            "--embed-resources",
            "-s",
            "--wrap=preserve",
            "-o",
        ]

        pandoc_args_list = [arg for arg in pandoc_args_list if arg]
        pandoc_args = " ".join(pandoc_args_list)

        if document_format == "pdf":
            return self.__convert_to_pdf(pandoc_args, output_file)

        stderr = ""
        try:
            _success, _stdout, stderr, status = GLib.spawn_command_line_sync(
                f"pandoc -t {document_format} {pandoc_args} {shlex.quote(output_path)}"
            )

            GLib.spawn_check_wait_status(status)

        except GLib.Error:
            logging.error("Cannot convert document: %s", stderr)
            return self.__convert_fail()

        return self.__convert_success(output_file)

    def __convert_to_pdf(self, pandoc_args: str, output_file: Gio.File) -> None:
        """Converts the document to PDF through HTML."""
        try:
            tmp_file = Gio.File.new_tmp("XXXXXX.html")[0]
        except GLib.Error as error:
            logging.error("Cannot create temporary file: %s", error)
            return self.__convert_fail()

        if self.document_suffix == "html":
            try:
                self.document.copy(tmp_file, Gio.FileCopyFlags.OVERWRITE)
            except GLib.Error as error:
                logging.error("Cannot copy document: %s", error)
                return self.__convert_fail()

        else:
            if not (tmp_path := tmp_file.get_path()):
                logging.error("Cannot get path for temporary file")
                return self.__convert_fail()

            stderr = ""
            try:
                _success, _stdout, stderr, status = GLib.spawn_command_line_sync(
                    f'pandoc {pandoc_args} "{tmp_path}"'
                )

                GLib.spawn_check_wait_status(status)

            except GLib.Error:
                logging.error("Cannot convert document: %s", stderr)
                return self.__convert_fail()

        GLib.idle_add(
            print_to_pdf,
            tmp_file,
            output_file,
            lambda: self.__convert_success(output_file, tmp_file),
            lambda: self.__convert_fail(tmp_file),
        )

        return None
