# print.py
#
# Copyright 2024 kramo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Utilitites for printing documents."""

import sys
from pathlib import Path
from typing import Callable, Optional

from gi.repository import Gio, Gtk, WebKit

sys.tracebacklimit = 0


def __on_load_changed(
    view: WebKit.WebView,
    event: WebKit.LoadEvent,
    user_data: tuple[str, Optional[Callable[..., None]], Optional[Callable[..., None]]],
) -> None:
    if event != WebKit.LoadEvent.FINISHED:
        return

    dest, success_cb, fail_cb = user_data

    if (not (dest_basename := dest.get_basename())) or (
        not (dest_path := dest.get_path())
    ):
        if fail_cb:
            fail_cb()

        return

    dest_basename = Path(dest_basename).stem
    dest_parent = str(Path(dest_path).parent)

    settings = Gtk.PrintSettings.new()
    settings.set_printer("Print to File")
    settings.set(Gtk.PRINT_SETTINGS_OUTPUT_FILE_FORMAT, "pdf")
    settings.set(Gtk.PRINT_SETTINGS_OUTPUT_BASENAME, dest_basename)
    settings.set(Gtk.PRINT_SETTINGS_OUTPUT_DIR, dest_parent)

    op = WebKit.PrintOperation.new(view)
    op.set_print_settings(settings)
    op.print_()

    if success_cb:
        success_cb()


def print_to_pdf(
    html_document: Gio.File,
    dest: Gio.File,
    success_cb: Optional[Callable[..., None]] = None,
    fail_cb: Optional[Callable[..., None]] = None,
) -> None:
    """Prints `html_document` to `dest` as a PDF."""
    view = WebKit.WebView()
    view.connect("load-changed", __on_load_changed, (dest, success_cb, fail_cb))
    view.load_uri(html_document.get_uri())

    # HACK: Don’t question PyGObject
    raise RuntimeError("Ignore me")
