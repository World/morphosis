# supported_formats.py
#
# Copyright 2024 Jamie Gravendeel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Collection of formats, mime types, and file extensions."""
from collections import namedtuple

Format = namedtuple("Format", "format ext")

FORMATS = {
    "PDF": Format("pdf", "pdf"),
    "Markdown": Format("markdown_strict", "md"),
    "reStructuredText": Format("rst", "rst"),
    "LaTeX": Format("latex", "tex"),
    "HTML": Format("html5", "html"),
    "Microsoft Word": Format("docx", "docx"),
    "OpenOffice/LibreOffice": Format("odt", "odt"),
    "Rich Text Format": Format("rtf", "rtf"),
    "EPUB": Format("epub3", "epub"),
    "AsciiDoc": Format("asciidoc", "adoc"),
}

# These MIME types make me feel like a mime
MIME_TYPES = (
    "text/markdown",
    "text/x-rst",
    "text/html",
    "text/x-tex",
    "application/vnd.oasis.opendocument.text",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "text/x-txt2tags",
    "application/epub+zip",
    "application/x-zip-compressed-fb2",
    "application/x-fictionbook+xml",
    "application/x-fictionbook",
    "application/docbook+xml",
    "application/x-docbook+xml",
    "application/vnd.oasis.docbook+xml",
    "text/x-opml+xml",
    "text/x-opml",
    "text/x-bibtex",
    "application/rtf",
    "application/x-ipynb+json",
    "text/x-typst",
    "text/csv",
    "text/x-comma-separated-values",
    "text/x-csv",
    "text/tab-separated-values",
    "application/x-research-info-systems",
    "text/x-wiki",
)

EXTENSIONS = ("org", "muse", "ris", "html")
